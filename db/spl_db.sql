create table contractors (
    id serial primary key,
    name varchar,
    email varchar,
    phone varchar,
    notes varchar,
    created timestamptz,
    updated timestamptz);

create table roles (id serial primary key, name varchar);
create table users (
    contractor_id int references contractors on update cascade on delete cascade,
    id serial primary key,
    name varchar,
    username varchar unique,
    password varchar,
    email varchar,
    role_id int references roles on update cascade on delete set null,
    created timestamptz,
    updated timestamptz);

create table stock_products (
    contractor_id int references contractors on update cascade on delete cascade,
    id serial primary key,
    name varchar,
    unit varchar,
    price numeric,
    user_id int references users on update cascade on delete set null,
    created timestamptz,
    updated timestamptz);
create table stock_registries (
    contractor_id int references contractors on update cascade on delete cascade,
    id serial primary key,
    stock_product_id int references stock_products on update cascade on delete cascade,
    quantity numeric,
    user_id int references users on update cascade on delete set null,
    created timestamptz,
    updated timestamptz);

create table clients (
    contractor_id int references contractors on update cascade on delete cascade,
    id serial primary key,
    name varchar,
    address varchar,
    phone varchar,
    user_id int references users on update cascade on delete set null,
    created timestamptz,
    updated timestamptz);

create table sale_products (
    contractor_id int references contractors on update cascade on delete cascade,
    id serial primary key,
    name varchar,
    unit varchar,
    price numeric,
    user_id int references users on update cascade on delete set null,
    created timestamptz,
    updated timestamptz);
create table sale_registries (
    contractor_id int references contractors on update cascade on delete cascade,
    id serial primary key,
    user_id int references users on update cascade on delete set null,
    client_id int references clients on update cascade on delete set null,
    created timestamptz,
    updated timestamptz);
create table sale_items (
    sale_registry_id int references sale_registries on update cascade on delete cascade,
    id serial primary key,
    sale_product_id int references sale_products on update cascade on delete cascade,
    unit varchar,
    unit_price numeric,
    quantity numeric,
    created timestamptz);

----------------------------------

insert into contractors (name, email, phone, notes) values ('Empazinado',
'admin@empanizado.com.br', '85 992919394', 'notes1');
insert into contractors (name, email, phone, notes) values ('Contractor 2',
'contractor2@contractor2.com', '85 992929394', 'notes2');
insert into contractors (name, email, phone, notes) values ('Contractor 3',
'contractor3@contractor3.com', '85 992939394', 'notes3');

insert into roles (name) values ('superadmin');
insert into roles (name) values ('admin');

insert into users (contractor_id, name, username, email, role_id, password) values
(NULL, 'Super Admin', 'superadmin', 'superadmin@superadmin.com', 1,
'{SSHA}esgz64CpHMo8pMfgIIszP13ft23z/zio04aCwNdm0wc6MDeloMUH4g==');
insert into users (contractor_id, name, username, email, role_id, password) values
(1, 'Admin Empazinado', 'empazinado', 'admin@empazinado.com.br', 2,
'{SSHA}esgz64CpHMo8pMfgIIszP13ft23z/zio04aCwNdm0wc6MDeloMUH4g==');
insert into users (contractor_id, name, username, email, role_id, password) values
(2, 'Admin 2', 'admin2', 'admin@contractor2.com', 2,
'{SSHA}esgz64CpHMo8pMfgIIszP13ft23z/zio04aCwNdm0wc6MDeloMUH4g==');
insert into users (contractor_id, name, username, email, role_id, password) values
(3, 'Admin 3', 'admin3', 'admin@contractor3.com', 2,
'{SSHA}esgz64CpHMo8pMfgIIszP13ft23z/zio04aCwNdm0wc6MDeloMUH4g==');

insert into clients (contractor_id, name, phone, address) values
(1, 'Client 1', '85 9392.1324', 'Endereço 1');
insert into clients (contractor_id, name, phone, address) values
(2, 'Client 2', '85 9492.1324', 'Endereço 2');
insert into clients (contractor_id, name, phone, address) values
(3, 'Client 3', '85 9342.1324', 'Endereço 3');

insert into sale_products (contractor_id, name, unit, price) values
(1, 'Hambúrguer de brócolis', 'unidade', 7),
(1, 'Suco (copo)', 'unidade', 4),
(1, 'Suco (jarra)', 'unidade', 7),

(1, 'Pastel de brócolis', 'unidade', 4),
(1, 'Pastel de castanha', 'unidade', 40),
(1, 'Pizza pequena', 'unidade', 20),

(1, 'Açaí', 'mg', 0.025),

(1, 'Guacamole', 'mg', 0.020),
(1, 'Tortilla', 'unidade', 9),
(1, 'Crepe amendoim', 'unidade', 11),

(1, 'Vitamina vegana', 'mg', 0.025),
(1, 'Bolo', 'fatia', 4),
(1, 'Sanduíche de quinoa', 'unidade', 15);
