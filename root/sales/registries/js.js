document.addEventListener('DOMContentLoaded', function() {
    $('select').selectize({ selectOnTab: true });
    $('#form-sale-registry-add-field').on('click', addSaleItem);
});

function addSaleItem (event) {
    var groups = document.querySelector('#form-sale-registry .sale-registry-group');

    var item = document.querySelector('#form-sale-registry .sale-registry-item');
    var selectedValue = item.querySelector('select').selectize.items[0];
    item.querySelector('select').selectize.destroy();

    var clonedItem = item.cloneNode(true);

    $( item.querySelector('select') ).selectize();
    item.querySelector('select').selectize.setValue(selectedValue);

    clonedItem.querySelectorAll('[name]').forEach(function (namedItem) {
        namedItem.value = '';
    });
    $(clonedItem.querySelectorAll('select')).selectize();
    groups.appendChild(clonedItem);

    document.querySelector('[name="sale_items_counter"]').value =
        parseInt(document.querySelector('[name="sale_items_counter"]').value) + 1;

    adjustNumbers('#form-sale-registry .sale-registry-item');
}

function adjustNumbers(itemClass) {
    var counter = 1;
    document.querySelectorAll(itemClass).forEach(function(item) {
        item.querySelectorAll('[name]').forEach(function (namedItem) {
            namedItem.name = namedItem.name.replace(/_\d+\./, '_' + counter + '.');
        });
        counter++;
    });
}
