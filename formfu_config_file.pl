use utf8;
{
    #languages => ['pt_br'],
    auto_container_class => 'form-group',
    auto_constraint_class => 'constraint_%t',
    #form_error_message => 'Erro no formulário. Por favor, confira seus campos.',
    #auto_error_class => 'error_message',
    default_args => {
        elements => {
            'Text|Password|Select' => {
                layout => ['label', 'errors', 'field', 'comment', 'javascript'],
                attrs => { class => 'form-control' },
            },
            Textarea => {
                layout => ['label', 'errors', 'field', 'comment', 'javascript'],
                attrs => { class => 'form-control font-family-mono' },
            },
        },
    },
}
