use utf8;
package SPL::Schema::Result::SaleRegistry;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';
__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");
__PACKAGE__->table("sale_registries");
__PACKAGE__->add_columns(
  "contractor_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "sale_registries_id_seq",
  },
  "user_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "client_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "created",
  { data_type => "timestamp with time zone", is_nullable => 1 },
  "updated",
  { data_type => "timestamp with time zone", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->belongs_to(
  "client",
  "SPL::Schema::Result::Client",
  { id => "client_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "SET NULL",
    on_update     => "CASCADE",
  },
);
__PACKAGE__->belongs_to(
  "contractor",
  "SPL::Schema::Result::Contractor",
  { id => "contractor_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "CASCADE",
    on_update     => "CASCADE",
  },
);
__PACKAGE__->has_many(
  "sale_items",
  "SPL::Schema::Result::SaleItem",
  { "foreign.sale_registry_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->belongs_to(
  "user",
  "SPL::Schema::Result::User",
  { id => "user_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "SET NULL",
    on_update     => "CASCADE",
  },
);


# Created by DBIx::Class::Schema::Loader v0.07045 @ 2017-09-06 13:01:06
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:XnE3/Sd96v0ulGJJtWb0TQ

__PACKAGE__->add_columns('+created', { set_on_create => 1 },
                         '+updated', { set_on_create => 1, set_on_update => 1 });

sub total_price {
    my $self = shift;
    my $total_price;
    for my $sale_item ($self->sale_items) {
        $total_price += $sale_item->unit_price * $sale_item->quantity;
    }
    return $total_price;
}

sub short_description {
    my $self = shift;
    my @sale_items;
    my $total_price;
    for my $sale_item ($self->sale_items) {
        push @sale_items,
          $sale_item->sale_product->name . ' x ' . $sale_item->quantity .
          ' (' . $sale_item->unit . ')';
        $total_price += $sale_item->unit_price * $sale_item->quantity;
    }
    return @sale_items;
}

# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
