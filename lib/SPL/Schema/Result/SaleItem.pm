use utf8;
package SPL::Schema::Result::SaleItem;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';
__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");
__PACKAGE__->table("sale_items");
__PACKAGE__->add_columns(
  "sale_registry_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "sale_items_id_seq",
  },
  "sale_product_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "unit",
  {
    data_type   => "text",
    is_nullable => 1,
    original    => { data_type => "varchar" },
  },
  "unit_price",
  { data_type => "numeric", is_nullable => 1 },
  "quantity",
  { data_type => "numeric", is_nullable => 1 },
  "created",
  { data_type => "timestamp with time zone", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->belongs_to(
  "sale_product",
  "SPL::Schema::Result::SaleProduct",
  { id => "sale_product_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "CASCADE",
    on_update     => "CASCADE",
  },
);
__PACKAGE__->belongs_to(
  "sale_registry",
  "SPL::Schema::Result::SaleRegistry",
  { id => "sale_registry_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "CASCADE",
    on_update     => "CASCADE",
  },
);


# Created by DBIx::Class::Schema::Loader v0.07045 @ 2017-09-07 12:32:34
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:NBPkdMQ/v6dO3DNpTOTWqA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
