package SPL::Utils;
use strict;
use warnings;
use HTML::FormFu;

sub formfu_creator {
    my ( $self, $otherself, $c, $resultset_name ) = @_;
    my $form = HTML::FormFu->new( $otherself->formfu_config );
    $form->stash->{schema} = $c->model('DB')->schema;
    $form->model_config->{resultset} = $resultset_name;
    $form->process( $c->req->params );

    return $form;
}

sub formfu_error_str_maker {
    my ( $self, $form ) = @_;
    my @errors;
    for my $error ( @{ $form->get_errors } ) {
        push @errors, "${ \$error->parent->label }: ${ \$error->message }."
    }
    return join ' ', @errors;
}

1;
