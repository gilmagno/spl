package SPL::Controller::Sales;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller'; }

sub base :Chained('/') PathPart('sales') CaptureArgs(0) {
    my ( $self, $c ) = @_;
}

sub index :Chained('base') PathPart('') Args(0) {}

__PACKAGE__->meta->make_immutable;

1;
