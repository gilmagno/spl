package SPL::Controller::Clients;
use Moose;
use namespace::autoclean;
use SPL::Utils;

BEGIN { extends 'Catalyst::Controller'; }

sub base :Chained('/') PathPart('clients') CaptureArgs(0) {
    my ( $self, $c ) = @_;
}

sub object :Chained('base') PathPart('') CaptureArgs(1) {
    my ( $self, $c, $id ) = @_;

    if (my $client = $c->model('DB::Client')->find( $id )) {
        $c->stash(client => $client);
    }
    else {
        $c->flash(error => 'Error fechting client.');
        $c->res->redirect( $c->uri_for_action('/clients/index') );
        $c->detach;
    }
}

sub index :Chained('base') PathPart('') Args(0) {
    my ( $self, $c ) = @_;
    my $criteria;
    if (!$c->user->is_superadmin) {
        $criteria = { contractor_id => $c->user->contractor_id }
    }

    my $c_rs = $c->model('DB::Client')->search_rs( $criteria,
       { page => $c->req->params->{page} || 1, order_by => { -asc => 'name' } }
    );

    $c->stash(clients => [ $c_rs->all ],
              pager   => $c_rs->pager);
}

sub details :Chained('object') PathPart('') Args(0) {
    my ( $self, $c ) = @_;
    if ( !$c->user->superadmin_or_same_contractor($c->stash->{client}) ) {
        $c->detach('/unauthorized');
    }
}

sub add :Chained('base') PathPart('add') Args(0) {
    my ( $self, $c ) = @_;
    my $form = SPL::Utils->formfu_creator( $self, $c, 'Client' );
    if (!$c->user->is_superadmin) {
        $form->remove_element( $form->get_element({ name => 'contractor_id' }) );
    }
    $form->process;

    if ($form->submitted) {
        if ($form->has_errors) {
            $c->flash(error =>
             'Error. ' . SPL::Utils->formfu_error_str_maker($form));
        }
        else {
            if (!$c->user->is_superadmin) {
                $form->add_valid('contractor_id', $c->user->contractor_id);
            }
            my $client = $form->model->create;
            $c->flash(success => 'Client added.');
            $c->res->redirect( $c->uri_for_action('/clients/details',
                                                  [$client->id]) );
        }
    }

    $c->stash(form => $form);
}

sub edit :Chained('object') PathPart('edit') Args(0) {
    my ( $self, $c ) = @_;
    my $form = SPL::Utils->formfu_creator( $self, $c, 'Client' );
    if (!$c->user->is_superadmin) {
        $form->remove_element( $form->get_element({ name => 'contractor_id' }) );
    }
    $form->process;

    if ($form->submitted) {
        if ($form->has_errors) {
            $c->flash(error =>
             'Error. ' . SPL::Utils->formfu_error_str_maker($form));
        }
        else {
            $form->model->update( $c->stash->{client} );
            $c->flash(success => 'Client edited.');
            $c->res->redirect( $c->uri_for_action('/clients/details',
                                                  [$c->stash->{client}->id]) );
        }
    }
    else {
        $form->model->default_values( $c->stash->{client} );
    }

    $c->stash(form => $form);
}

sub delete :Chained('object') PathPart('delete') Args(0) {
    my ( $self, $c ) = @_;

    if ($c->req->method eq 'POST') {
        $c->stash->{client}->delete;
        $c->flash(success => 'Client deleted');
        $c->res->redirect( $c->uri_for_action('/clients/index') );
    }
}

sub formfu_config {
    return {
        load_config_file => 'formfu_config_file.pl',
        elements => [
            { type => 'Select',
              name => 'contractor_id',
              label => 'Contractor',
              model_config => { resultset => 'Contractor' } },
            { type => 'Text',
              name => 'name',
              label => 'Name',
              constraints => 'Required' },
            { type => 'Text',
              name => 'phone',
              label => 'Phone' },
            { type => 'Textarea',
              name => 'address',
              label => 'Address',
              attrs => { rows => 5 } },
            { type => 'Submit' },
        ],
    },
}

__PACKAGE__->meta->make_immutable;

1;
