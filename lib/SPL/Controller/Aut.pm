package SPL::Controller::Aut;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller'; }

=head1 NAME

SPL::Controller::Aut - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub login :Path('login') Args(0) {
    my ( $self, $c ) = @_;

    if ($c->req->method eq 'POST') {
        my $username = $c->req->params->{username};
        my $password = $c->req->params->{password};
        my $auth_data = { username => $username, password => $password };
        if ( $username && $password && $c->authenticate($auth_data) ) {
            if ( $c->req->params->{'keep-logged'} ) {
                $c->change_session_expires( 30 * 24 * 60 * 60 );
            }
            $c->res->redirect( $c->uri_for_action('/index') );
        }
        else {
            $c->flash(error => 'Login failed.');
            $c->res->redirect( $c->uri_for_action('/aut/login') );
        }
    }
}

sub logout :Path('logout') Args(0) {
    my ( $self, $c ) = @_;
    $c->logout;
    $c->res->redirect( $c->uri_for_action('/aut/login') );
}

sub password_recovery :Path('/aut/password-recovery') {
    my ($self, $c) = @_;
    # captcha
    if ($c->req->method eq 'POST') {
        my $user = $c->model('DB::User')->find($c->req->params->{username});
        if (!$user) {
            $c->flash->{error} = 'Wrong data.';
        }
        else {
            use UUID::Tiny qw/create_uuid_as_string/;
            use DateTime;
            $user->password_recovery_token( create_uuid_as_string );
            $user->password_recovery_time( DateTime->now );
            $user->update;
            $c->flash->{success} = 'Instructions were sent to your email.';
        }
        $c->res->redirect( $c->uri_for_action('/aut/password_recovery') );
    }
}

sub password_recovery_confirmation :Path('/aut/password-recovery-confirmation') {
    my ($self, $c) = @_;

    if ($c->req->method eq 'POST') {
        # captcha
        my $user = $c->model('DB::User')->find($c->req->params->{username});

        if (!$user) {
            $c->flash->{error} = 'Wrong data.';
        }
        else {
            my $minutes = DateTime->now->delta_ms
              ( $user->password_recovery_time )->delta_minutes;

            if ($minutes > 24*60
                 || $user->password_recovery_token ne $c->req->params->{token}) {
                $c->flash->{error} =
                  "Can't use the given token. Please, request a new one."
            }
            elsif ( !$c->req->params->{password1}
              || $c->req->params->{password1} ne $c->req->params->{password2} ) {
                $c->flash->{error} =
                  "You should correctly inform the new password twice."
            }
            else {
                $user->password( $c->req->params->{password1} );
                $user->update;
                $c->flash->{success} =
                  "Password reset. Go to login page and log in."
            }
        }

        $c->res->redirect($c->req->uri_with);
    }
}

=encoding utf8

=head1 AUTHOR

sgrs,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
