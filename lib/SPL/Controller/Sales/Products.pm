package SPL::Controller::Sales::Products;
use Moose;
use namespace::autoclean;
use SPL::Utils;

BEGIN { extends 'Catalyst::Controller'; }

sub base :Chained('/sales/base') PathPart('products') CaptureArgs(0) {
    my ( $self, $c ) = @_;
}

sub object :Chained('base') PathPart('') CaptureArgs(1) {
    my ( $self, $c, $id ) = @_;

    if (my $sale_prod = $c->model('DB::SaleProduct')->find( $id )) {
        $c->stash(sale_prod => $sale_prod);
    }
    else {
        $c->flash(error => 'Error fechting sale product.');
        $c->res->redirect( $c->uri_for_action('/sales/products/index') );
        $c->detach;
    }
}

sub index :Chained('base') PathPart('') Args(0) {
    my ( $self, $c ) = @_;
    my $criteria;
    if (!$c->user->is_superadmin) {
        $criteria = { contractor_id => $c->user->contractor_id }
    }

    my $sale_prods_rs = $c->model('DB::SaleProduct')->search_rs( $criteria,
       { page => $c->req->params->{page} || 1, order_by => { -asc => 'name' } }
    );

    $c->stash(sale_prods => [ $sale_prods_rs->all ],
              pager   => $sale_prods_rs->pager);
}

sub details :Chained('object') PathPart('') Args(0) {
    my ( $self, $c ) = @_;
    if ( !$c->user->superadmin_or_same_contractor($c->stash->{sale_prod}) ) {
        $c->detach('/unauthorized');
    }
}

sub add :Chained('base') PathPart('add') Args(0) {
    my ( $self, $c ) = @_;
    my $form = SPL::Utils->formfu_creator( $self, $c, 'SaleProduct' );
    $form->process;

    if ($form->submitted) {
        if ($form->has_errors) {
            $c->flash(error =>
             'Error. ' . SPL::Utils->formfu_error_str_maker($form));
        }
        else {
            if (!$c->user->is_superadmin) {
                $form->add_valid('contractor_id', $c->user->contractor_id);
            }
            my $sale_prod = $form->model->create;
            $c->flash(success => 'Sale product added.');
            $c->res->redirect( $c->uri_for_action('/sales/products/details',
                                                  [$sale_prod->id]) );
        }
    }

    $c->stash(form => $form);
}

sub edit :Chained('object') PathPart('edit') Args(0) {
    my ( $self, $c ) = @_;
    my $form = SPL::Utils->formfu_creator( $self, $c, 'SaleProduct' );
    $form->process;

    if ($form->submitted) {
        if ($form->has_errors) {
            $c->flash(error =>
             'Error. ' . SPL::Utils->formfu_error_str_maker($form));
        }
        else {
            $form->model->update( $c->stash->{sale_prod} );
            $c->flash(success => 'Sale product edited.');
            $c->res->redirect( $c->uri_for_action('/sales/products/details',
                                                  [$c->stash->{sale_prod}->id]) );
        }
    }
    else {
        $form->model->default_values( $c->stash->{sale_prod} );
    }

    $c->stash(form => $form);
}

sub delete :Chained('object') PathPart('delete') Args(0) {
    my ( $self, $c ) = @_;

    if ($c->req->method eq 'POST') {
        $c->stash->{sale_prod}->delete;
        $c->flash(success => 'Sale product deleted');
        $c->res->redirect( $c->uri_for_action('/sales/products/index') );
    }
}

sub formfu_config {
    return {
        load_config_file => 'formfu_config_file.pl',
        elements => [
            { type => 'Text',
              name => 'name',
              label => 'Name',
              constraints => 'Required' },
            { type => 'Text',
              name => 'unit',
              label => 'Unit',
              constraints => 'Required' },
            { type => 'Text',
              name => 'price',
              label => 'Price',
              constraints => 'Required' },
            { type => 'Submit' },
        ],
    },
}

__PACKAGE__->meta->make_immutable;

1;
