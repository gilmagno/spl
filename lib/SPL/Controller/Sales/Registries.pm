package SPL::Controller::Sales::Registries;
use utf8;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller'; }

sub base :Chained('/sales/base') PathPart('registries') CaptureArgs(0) {
    my ( $self, $c ) = @_;
}

sub object :Chained('base') PathPart('') CaptureArgs(1) {
    my ( $self, $c, $id ) = @_;

    if (my $sale_reg = $c->model('DB::SaleRegistry')->find( $id )) {
        $c->stash(sale_reg => $sale_reg);
    }
    else {
        $c->flash(error => 'Error fechting sale registry.');
        $c->res->redirect( $c->uri_for_action('/sales/registries/index') );
        $c->detach;
    }
}

sub index :Chained('base') PathPart('') Args(0) {
    my ( $self, $c ) = @_;
    my $criteria;
    if (!$c->user->is_superadmin) {
        $criteria = { 'me.contractor_id' => $c->user->contractor_id }
    }

    my $sale_regs_rs = $c->model('DB::SaleRegistry')->search_rs
      ( $criteria,
        { prefetch => [ { 'sale_items' => { 'sale_product' => 'contractor' } }, 'contractor' ],
          page => $c->req->params->{page} || 1, order_by => { -desc => 'me.id' } } );

    $c->stash(sale_regs => [ $sale_regs_rs->all ],
              pager => $sale_regs_rs->pager);

}

sub details :Chained('object') PathPart('') Args(0) {
    my ( $self, $c ) = @_;
    if ( !$c->user->superadmin_or_same_contractor($c->stash->{sale_reg}) ) {
        $c->detach('/unauthorized');
    }
}

sub add :Chained('base') PathPart('add') Args(0) {
    my ( $self, $c ) = @_;
    my $form = HTML::FormFu->new( formfu_config($c->user->contractor_id) );
    $form->stash->{schema} = $c->model('DB')->schema;
    $form->model_config->{resultset} = 'SaleRegistry';
    $form->process( $c->req->params );

    if ($form->submitted) {
        if ($form->has_errors) {
            $c->flash(error =>
             'Error. ' . SPL::Utils->formfu_error_str_maker($form));
        }
        else {
            $form->add_valid('contractor_id', $c->user->contractor_id);
            my $sale_reg = $form->model->create;
            for my $item ($sale_reg->sale_items) {
                $item->unit( $item->sale_product->unit );
                $item->unit_price( $item->sale_product->price );
                $item->update;
            }
            $c->flash(success => 'Sale registry added.');
            $c->res->redirect( $c->uri_for_action('/sales/registries/details',
                                                  [$sale_reg->id]) );
        }
    }
    else {
        $form->get_all_element({ type => 'Repeatable' })->repeat(10);
        $form->get_all_element('sale_items_counter')->default(10);
        $form->process;
    }

    $c->stash(form => $form);
}

################

sub edit :Chained('object') PathPart('edit') Args(0) {
    my ( $self, $c ) = @_;
    my $form = HTML::FormFu->new( formfu_config($c->user->contractor_id) );
    $form->stash->{schema} = $c->model('DB')->schema;
    $form->model_config->{resultset} = 'SaleRegistry';
    $form->process( $c->req->params );

    if ($form->submitted) {
        if ($form->has_errors) {
            $c->flash(error =>
             'Error. ' . SPL::Utils->formfu_error_str_maker($form));
        }
        else {
            $form->model->update( $c->stash->{sale_prod} );
            $c->flash(success => 'Sale product edited.');
            $c->res->redirect( $c->uri_for_action('/sales/products/details',
                                                  [$c->stash->{sale_prod}->id]) );
        }
    }
    else {
        $form->model->default_values( $c->stash->{sale_prod} );
    }

    $c->stash(form => $form);
}

####

sub delete :Chained('object') PathPart('delete') Args(0) {
    my ( $self, $c ) = @_;

    if ($c->req->method eq 'POST') {
        $c->stash->{sale_reg}->delete;
        $c->flash(success => 'Sale registry deleted');
        $c->res->redirect( $c->uri_for_action('/sales/registries/index') );
    }
}

sub formfu_config {
    my $contractor_id = shift;
    return {
        load_config_file => 'formfu_config_file.pl',
        attrs => { id => 'form-sale-registry' },
        elements => [
            { type => 'Hidden',
              name => 'sale_items_counter' },
            { type => 'Repeatable',
              nested_name => 'sale_items',
              counter_name => 'sale_items_counter',
              model_config => { empty_rows => '3', new_rows_max => '500' },
              elements => [
                  { type => 'Hidden',
                    name => 'id' },
                  { type => 'Select',
                    name => 'sale_product_id',
                    label => 'Product',
                    empty_first => 1,
                    model_config => {
                        resultset => 'SaleProduct',
                        ignore_if_empty => 1,
                        condition => { contractor_id => $contractor_id, },
                        attributes => {
                            order_by => { -asc => 'name' } }
                    } },
                  { type => 'Text',
                    name => 'quantity',
                    label => 'Quantity',
                    constraints => 'Number',
                    model_config => { delete_if_empty => 1 } },
                  { type => 'Checkbox',
                    name => 'delete_sale_item',
                    model_config => { delete_if_true => 1 } }
              ], },
            { type => 'Submit',
              value => 'Submit',
              attrs => { class => 'btn btn-primary' } },
        ],
    },
}

__PACKAGE__->meta->make_immutable;

1;
