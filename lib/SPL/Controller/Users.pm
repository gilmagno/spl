package SPL::Controller::Users;
use Moose;
use namespace::autoclean;
use SPL::Utils;

BEGIN { extends 'Catalyst::Controller'; }

sub base :Chained('/') PathPart('users') CaptureArgs(0) {
    my ( $self, $c ) = @_;
}

sub object :Chained('base') PathPart('') CaptureArgs(1) {
    my ( $self, $c, $id ) = @_;

    if (my $user = $c->model('DB::User')->find( $id )) {
        $c->stash(user => $user);
        if ( !$c->user->superadmin_or_same_contractor($c->stash->{user}) ) {
            $c->detach('/default');
        }
    }
    else {
        $c->flash(error => 'Error fechting user.');
        $c->res->redirect( $c->uri_for_action('/users/index') );
        $c->detach;
    }
}

sub index :Chained('base') PathPart('') Args(0) {
    my ( $self, $c ) = @_;
    my $criteria;
    if (!$c->user->is_superadmin) {
        $criteria = { contractor_id => $c->user->contractor_id }
    }

    my $u_rs = $c->model('DB::User')->search_rs( $criteria,
       { page => $c->req->params->{page} || 1, order_by => { -asc => 'name' } }
    );

    $c->stash(users => [ $u_rs->all ],
              pager => $u_rs->pager);
}

sub details :Chained('object') PathPart('') Args(0) {
    my ( $self, $c ) = @_;
}

sub add :Chained('base') PathPart('add') Args(0) {
    my ( $self, $c ) = @_;
    my $form = SPL::Utils->formfu_creator( $self, $c, 'User' );
    $form->get_element({ name=> 'password' })->constraints('Required');
    $self->_insert_roles_in_form($c, $form);
    if (!$c->user->is_superadmin) {
        $form->remove_element( $form->get_element({ name => 'contractor_id' }) );
    }
    $form->process;

    if ($form->submitted) {
        if ($form->has_errors) {
            $c->flash(error =>
             'Error. ' . SPL::Utils->formfu_error_str_maker($form));
        }
        else {
            if (!$c->user->is_superadmin) {
                $form->add_valid('contractor_id', $c->user->contractor_id);
            }
            my $user = $form->model->create;
            $c->flash(success => 'User added.');
            $c->res->redirect( $c->uri_for_action('/users/details',
                                                  [$user->id]) );
        }
    }

    $c->stash(form => $form);
}

sub edit :Chained('object') PathPart('edit') Args(0) {
    my ( $self, $c ) = @_;
    my $form = SPL::Utils->formfu_creator( $self, $c, 'User' );
    $form->get_element({ name => 'password' })
      ->model_config({ ignore_if_empty => 1 });
    $form->stash->{self_stash_key} = $c->stash->{user};
    $self->_insert_roles_in_form($c, $form);
    $form->process;

    if ($form->submitted) {
        if ($form->has_errors) {
            $c->flash(error =>
             'Error. ' . SPL::Utils->formfu_error_str_maker($form));
        }
        else {
            $form->model->update( $c->stash->{user} );
            $c->flash(success => 'User edited.');
            $c->res->redirect( $c->uri_for_action('/users/details',
                                                  [$c->stash->{user}->id]) );
        }
    }
    else {
        $form->model->default_values( $c->stash->{user} );
    }

    $c->stash(form => $form);
}

sub delete :Chained('object') PathPart('delete') Args(0) {
    my ( $self, $c ) = @_;

    if ($c->req->method eq 'POST') {
        $c->stash->{user}->delete;
        $c->flash(success => 'User deleted');
        $c->res->redirect( $c->uri_for_action('/users/index') );
    }
}

sub _insert_roles_in_form {
    my ( $self, $c, $form ) = @_;
    my $criteria;
    if (!$c->user->is_superadmin) {
        $criteria = { name => { '!=' => 'superadmin' } }
    }
    my @roles = $c->model('DB::Role')->search
      ($criteria, { order_by => { -asc => 'name' } });
    my @roles_options = map { [ $_->id, $_->name ] } @roles;
    $form->get_element({ name => 'role_id' })->options(\@roles_options)
}

sub formfu_config {
    return {
        load_config_file => 'formfu_config_file.pl',
        elements => [
            { type => 'Select',
              name => 'contractor_id',
              label => 'Contractor',
              model_config => { resultset => 'Contractor' } },
            { type => 'Text',
              name => 'username',
              label => 'Username',
              constraints => [
                  'Required',
                  { type => 'DBIC::Unique',
                    resultset => 'User',
                    self_stash_key => 'self_stash_key' } ] },
            { type => 'Text',
              name => 'name',
              label => 'Name',
              constraints => 'Required' },
            { type => 'Text',
              name => 'email',
              label => 'Email' },
            { type => 'Select',
              name => 'role_id',
              label => 'Role' },
            { type => 'Password',
              name => 'password',
              label => 'Password',
              constraints => [
                  { type => 'Equal',
                    others => 'password_repeat' }, ], },
            { type => 'Password',
              name => 'password_repeat',
              label => 'Password again' },
            { type => 'Submit' },
        ],
    },
}

__PACKAGE__->meta->make_immutable;

1;
