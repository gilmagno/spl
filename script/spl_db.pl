#!/usr/bin/env perl

use v5.20;
use warnings;
use FindBin;

my $option = $ARGV[0] || 'null';

if ($option eq 'reset') {
    my $pw = 'spl';
    my $dbfile = $FindBin::Bin . '/../db/spl_db.sql';

    system("PGPASSWORD=$pw dropdb -h localhost -U spl spl");
    system("PGPASSWORD=$pw createdb -h localhost -U spl spl");
    system("PGPASSWORD=$pw psql -h localhost -U spl -f $dbfile spl");
}
elsif ($option eq 'dbicdump') {
    my $filename = $FindBin::Bin . '/spl_create.pl';

    my $cmd = <<"LINE";
$filename model DB DBIC::Schema SPL::Schema create=static 'dbi:Pg:dbname=spl;host=127.0.0.1' 'spl' 'spl' '{ loader_options => { use_moose => 1, moniker_map => { }, generate_pod => 0, components => ["InflateColumn::DateTime", "TimeStamp", "PassphraseColumn"] } }'
LINE

    system($cmd);
}
else {
    say "Can't understand option '$option'."
}
