use strict;
use warnings;
use Test::More;


use Catalyst::Test 'SPL';
use SPL::Controller::Sales::Products;

ok( request('/sales/products')->is_success, 'Request should succeed' );
done_testing();
