use v5.20;
use warnings;
use Test::More;
use Test::WWW::Mechanize::Catalyst 'SPL';

my $mech = Test::WWW::Mechanize::Catalyst->new;

$mech->get_ok('/');
$mech->base_like(qr{aut/login});
$mech->post('/aut/login', { username => 'superadmin', password => 'mypass' });
$mech->content_unlike(qr/Authorization denied/);

# index

$mech->get_ok('/users');
$mech->content_like(qr/<h1>Users<\/h1>/);
# add

$mech->get_ok('/users/add');

my $username = int(rand(10)) . int(rand(10)) . int(rand(10))
       . int(rand(10)) . int(rand(10)) . int(rand(10));

$mech->post('/users/add',
  { name => '', password => 'qwe1', password_repeat => 'qwe2' });

$mech->content_like(qr/Username: This field is required/);
$mech->content_like(qr/Password again: Does not match 'Password' value/);

$mech->post('/users/add',
  { username => $username, password => 'qwe', password_repeat => 'qwe' });

$mech->content_like(qr/User added/);

my ($id) = $mech->uri =~ /(\d+)$/;

$mech->post('/users/add',
  { username => $username, password => 'qwe', password_repeat => 'qwe' });

$mech->content_like(qr/Username: Value already exists in database/);

$mech->post('/users/add', { name => 123 });
$mech->content_like(qr{Username: This field is required});

# details

$mech->get_ok("/users/$id");
$mech->follow_link( text => 'Edit' );

# edit

$mech->content_like(qr/User edit/);

$mech->post("/users/$id/edit", { username => "$username$username" });
$mech->content_like(qr/User edited/);

$mech->post("/users/$id/edit", { username => "$username" });
$mech->content_like(qr/User edited/);

# delete

$mech->get("/users/$id/delete");
$mech->content_like(qr/User delete/);
$mech->post("/users/$id/delete");
$mech->content_like(qr/User deleted/);
$mech->content_like(qr/<h1>Users<\/h1>/);

done_testing();
