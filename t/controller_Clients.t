use strict;
use warnings;
use Test::More;


use Catalyst::Test 'SPL';
use SPL::Controller::Clients;

ok( request('/clients')->is_success, 'Request should succeed' );
done_testing();
