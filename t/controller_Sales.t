use strict;
use warnings;
use Test::More;


use Catalyst::Test 'SPL';
use SPL::Controller::Sales;

ok( request('/sales')->is_success, 'Request should succeed' );
done_testing();
