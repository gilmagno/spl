use strict;
use warnings;
use Test::More;


use Catalyst::Test 'SPL';
use SPL::Controller::Sales::Registries;

ok( request('/sales/registries')->is_success, 'Request should succeed' );
done_testing();
