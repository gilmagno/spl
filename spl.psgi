use strict;
use warnings;

use SPL;

my $app = SPL->apply_default_middlewares(SPL->psgi_app);
$app;

